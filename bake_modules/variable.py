class Variable:
	def __init__(self, data, values):
		self.data = data
		self.values = values
	def toDict(self):
		returnDict = {}
		for item in self.data:
			returnDict[item] = self.values[item] if item in self.values else self.data[item]
		return returnDict