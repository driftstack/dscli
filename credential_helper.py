import logging
import os
from pathlib import Path
import sys
from config import Config
import requests 
 
dsHome = os.path.join(Path.home(),".ds")
credentialHelperLog = os.path.join(dsHome,"git-credential-helper.log")

def createFiles():
    if not os.path.exists(dsHome):
        os.makedirs(dsHome)
    if not os.path.exists(credentialHelperLog):
        with open(credentialHelperLog, 'w') as f:
            f.write("")
createFiles() 
FORMAT = '[%(asctime)s] %(message)s'

logging.basicConfig(format=FORMAT, filename=credentialHelperLog, level=logging.INFO)

class CredentialHelper():
    def __init__(self):
        self.protocol = ""
        self.host = ""
        self.password = ""
        self.username = ""
        self.config = Config()

    def store(self):
        logging.info("store")
        self.setVars()
    def get(self):
        logging.info("get")
        self.setVars()
        self.requestCreds()
        self.printCreds()
    def erase(self):
        logging.info("erase")

    def setVars(self):
        for line in sys.stdin:
            logging.info(line)
            if "protocol=" in line:
                self.protocol = line.split("=",1)[1].strip()
            if "host=" in line:
                self.host = line.split("=",1)[1].strip()
            if "username=" in line:
                self.username = line.split("=",1)[1].strip()
            if "password=" in line:
                self.password = line.split("=",1)[1].strip()
    def printCreds(self):
        logging.info("return host: "+self.host)
        logging.info("return protocol: "+self.protocol)
        logging.info("return password: "+self.password)
        logging.info("return username: "+self.username)
        print("host="+self.host)
        print("protocol="+self.protocol)
        print("password="+self.password)
        print("username="+self.username)

    def requestCreds(self):
        url = self.config.get('api_url')+'providers/credential-helper'
        r = requests.post(url, 
            headers={
                'Authorization': 'Bearer '+self.config.get('token')
            }, json={'host': self.host})
        logging.info("json "+r.content.decode("utf-8"))
        self.password = r.json()['password']
        self.username = r.json()['username']