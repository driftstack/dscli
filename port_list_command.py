import requests
from config import Config
import json
from tabulate import tabulate

class PortListCommand:
    def __init__(self, all=False):
        self.config = Config()
        self.all = all

    def run(self):
        url = self.config.get('api_url')+'instance-ports'
        r = requests.get(url, 
            headers={
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+self.config.get('token')
            })
        table = []
        for portData in r.json():
            if portData['uid'] != self.config.get('uid') and not self.all: 
                continue
            name = portData['name'] if (portData['name'] is not None and portData['name'] != "") else "port"+str(portData['port'])
            url = "https://"+name+"-"+portData['uid']+"."+self.config.get("workspace_domain")
            access_level = "private"
            if int(portData['access_level']) == 1:
                access_level = "internal"
            if int(portData['access_level']) == 2:
                access_level = "public"
            if portData['share_token']:
                url = url+"?token="+portData['share_token']
            table.append([portData['port'], access_level, url])
        
        print(tabulate(table, headers=["port", "access_level", "url"]))
